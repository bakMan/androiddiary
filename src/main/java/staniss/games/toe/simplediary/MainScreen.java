package staniss.games.toe.simplediary;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import staniss.games.toe.simplediary.db.DBAdapter;
import staniss.games.toe.simplediary.db.DBChangeManager;
import staniss.games.toe.simplediary.db.DBOpenHelper;
import staniss.games.toe.simplediary.db.GUIRecordMapper;
import staniss.games.toe.simplediary.fragments.EventListFragment;
import staniss.games.toe.simplediary.fragments.SlidePagerAdapter;
import staniss.games.toe.simplediary.fragments.WelcomeFragment;
import staniss.games.toe.simplediary.fragments.WelcomePagerAdapter;

/** Main application activity
 *
 */
public class MainScreen extends Activity implements EventListFragment.ListClickListener, DBChangeManager.DBChangeListener, DBChangeManager.DBEmptinessListener {

    static final String TAG = "MAINSCREEN";

    //ordering settings
    static final String ORD_MODE_ASC = "ASC";
    static final String ORD_MODE_DESC = "DESC";
    static final String ORD_KEY = "ordering";
    static final String ORD_MODE_KEY = "ordering_mode";

    //APP -> db adapter
    DBAdapter db;

    //ViewPagers -> one for showing only welcome screen, other for listing of events
    WelcomePagerAdapter welcome_adapter;
    SlidePagerAdapter lists_adapter;


    FragmentManager fragment_manager;
    ViewPager pager;

    //Default sorting setting
    String sort_column = DBOpenHelper.DATE_FIELD;
    String sort_mode = ORD_MODE_ASC;

    //state flags
    boolean is_welcome_screen = false;
    boolean is_db_empty = false;
    boolean ACTIVITY_RESUMED = false;

    //List of time ranges for db queries of individual list on the main screen
    DBAdapter.TIME_FRAME[] time_ranges = {DBAdapter.TIME_FRAME.TODAY, DBAdapter.TIME_FRAME.THIS_WEEK,DBAdapter.TIME_FRAME.FUTURE, DBAdapter.TIME_FRAME.PAST};
    //titles of the lists on the main screen
    String[] page_titles = {"Today","This week","Upcoming","Past"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        //create db connection
        DBChangeManager.set_context(getApplicationContext());
        db = new DBAdapter(this);
        db.open();

        //save fragment manager
        fragment_manager =  getFragmentManager();

        welcome_adapter = new WelcomePagerAdapter(new WelcomeFragment.IFirstEventListener() {
                @Override
                public void onFirstItemAdd() {
                    startEdit(GUIRecordMapper.NO_ID);
                }},fragment_manager);

        pager = (ViewPager) findViewById(R.id.pager_area);

        //set up pager of lists
        Cursor[] curs = createCursors();
        lists_adapter = new SlidePagerAdapter(curs,page_titles,fragment_manager);
        lists_adapter.setListListener(this);

        //vertical -> create one fragment
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            //VERTICAL (PORTRAIT) ORIENTATION

            if(db.getEventsNum() == 0)
            {
                pager.setAdapter(welcome_adapter);
                is_welcome_screen = true;
                is_db_empty = true;

            }
            else
            {
                pager.setAdapter(lists_adapter);
            }
//            //we are listening for clicks
//            list.setListener(this);

        }
        else
        {
            //Future special Horizontal gui

        }

        //register for db changes
        DBChangeManager.getMgr().register(this);
        DBChangeManager.getMgr().emp_register(this);

        //register add button action
        Button b = (Button) findViewById(R.id.btn_add);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startEdit(GUIRecordMapper.NO_ID);
            }
        });

    }

    //Create cursor based on the time ranges intended for main screen lists
    private Cursor[] createCursors(){

        Cursor[] res = new Cursor[time_ranges.length];

        int cnt=0;
        for(DBAdapter.TIME_FRAME frame : time_ranges)
        {
            res[cnt] = db.getAllEvents(sort_column,sort_mode.equals("ASC"),frame);
            cnt++;
        }

        return res;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        ACTIVITY_RESUMED = true;
        refreshView();
        lists_adapter.notifyDataSetChanged();

        //load default sorting from the preference
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        //If nothing was loaded column and mode contain default values
        sort_column = prefs.getString(ORD_KEY,sort_column);
        sort_mode = prefs.getString(ORD_MODE_KEY,sort_mode);

        //if there are events -> create list fragment
        //list.setCursor(db.getAllEvents(sort_column,sort_mode.equals("ASC")));
        lists_adapter.setCursors(createCursors());


    }

    @Override
    protected void onPause() {
        super.onPause();
        ACTIVITY_RESUMED = false;
    }

    public void refreshView()
    {

        //if activity is in the bg, can't perform fragment transactions
        if(!ACTIVITY_RESUMED)
            return;;

        //welcome screen is visible, db is not empty now
        if(is_welcome_screen && !is_db_empty)
        {
            //show lists
            pager.setAdapter(lists_adapter);
            is_welcome_screen = false;
        }
        else if(!is_welcome_screen && is_db_empty)
        { //welcome screen is not visible, db became empty
            //show welcome screen again
            pager.setAdapter(welcome_adapter);
            is_welcome_screen = true;
        }
        else if(!is_welcome_screen)
        {
        }
        else
        {
            Log.w(TAG,"Bad state of welcome screen");
        }

        //refresh list viees
        lists_adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,PreferencesScreen.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void detail(long id) {
        Log.i(TAG,"User clicked on the list item with id " + Long.toString(id));

        //start detail activity with intent
        Intent intent = new Intent(this, DetailScreen.class);
        intent.putExtra("ID",id);
        startActivity(intent);
    }

    @Override
    public void onDBChange(long id) {
        Log.i(TAG,"Recieved DB change notifycation");
        //list.setCursor(db.getAllEvents(sort_column,sort_mode.equals("ASC")));
        lists_adapter.setCursors(createCursors());


    }

    public void startEdit(long ID)
    {
        Intent intent = new Intent(MainScreen.this, DetailScreen.class);
        intent.putExtra("ID",ID);
        intent.putExtra("EDITING",true);
        startActivity(intent);
    }

    @Override
    public void onDelete(long ID) {
        db.deleteEvent(ID);
        DBChangeManager.getMgr().notify(ID);
    }

    @Override
    public void onDBEmpty() {
        is_db_empty = true;
        refreshView(); // this will be executed only if view is resumed, otherwise refresh just returns
    }

    @Override
    public void onDBNonEmpty() {
        is_db_empty = false;
        refreshView();
    }
}
