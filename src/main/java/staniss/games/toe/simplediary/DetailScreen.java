package staniss.games.toe.simplediary;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import staniss.games.toe.simplediary.db.DBAdapter;
import staniss.games.toe.simplediary.db.DBChangeManager;
import staniss.games.toe.simplediary.fragments.ARecordFragment;
import staniss.games.toe.simplediary.fragments.DetailFragment;
import staniss.games.toe.simplediary.fragments.EditFragment;

/** Activity for detailed view -> contains both edit and detail fragment
 * Created by Stanislav Smatana on 27/04/2015.
 */
public class DetailScreen extends Activity implements DetailFragment.EditEventListener {

    String TAG = "DETAIL_ACTIVITY";
    final static String FRAGMENT_TAG = "DET_FRAG";

    //database interface
    DBAdapter db;

    //fragment manager and current fragment
    FragmentManager f_manager;
    ARecordFragment cur_fragment;

    EditFragment f_edit;
    DetailFragment f_det;

    //database id of record being edited
    long id;

    boolean is_editing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_screen);

        //get data
        Intent intent = getIntent();
        id = intent.getLongExtra("ID", -1);
        is_editing = intent.getBooleanExtra("EDITING", false);

        //No id for detail view was sent and viewing was requested -> error
        //in case of editing -1 is new item
        if (!is_editing && id == -1) {
            Log.e(TAG, "No event ID was provided in the intent !");
            finish();
        }


        db = new DBAdapter(this);
        db.open();

        f_det = setupDetailFragment();
        f_edit = setupEditFragment();

        //register fragments for db changes
        DBChangeManager db_mgr = DBChangeManager.getMgr();
        db_mgr.register(f_det);
        db_mgr.register(f_edit);

        //get manager of fragments
        f_manager = getFragmentManager();
        FragmentTransaction trans = f_manager.beginTransaction();

        //build fragment according to recieved arguments
        //THIS REPETITIVE CODE CANNOT BE AVOIDED
        if (is_editing)
        {
            //setup editing fragment
            cur_fragment = f_edit;

        }
        else
        {
            //setup detail fragment
            cur_fragment = f_det;

        }

        trans.add(R.id.fragment_frame,cur_fragment,FRAGMENT_TAG);
        trans.commit();

        cur_fragment.populate(id,db);


        //set callback for finishing the edit
        f_edit.setListener(new EditFragment.IFinishedEditingListener() {
            @Override
            public void onEditingFinished() {
                //was editing called directly from activity ?
                //if so save results in ending activity
                if(getIntent().getBooleanExtra("EDITING",false))
                {
                    finish();
                }
                else
                {
                    //otherwise, fragments are changed
                    f_manager.popBackStack();

                }
            }
        });
    }

    private EditFragment setupEditFragment()
    {
        EditFragment f =  new EditFragment();
        f.populate(id,db);
        return f;
    }

    private DetailFragment setupDetailFragment()
    {
        DetailFragment f = new DetailFragment();
        f.populate(id,db);
        f.setEditListener(this);
        return f;
    }

    @Override
    public void onEdit() {


        //change fragment to editing
        FragmentTransaction trans = f_manager.beginTransaction();
        trans.replace(R.id.fragment_frame,f_edit);
        trans.addToBackStack(null);
        trans.commit();

        cur_fragment = f_edit;
        is_editing = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //on destruction fragments must be unregistered from db change listening, otherwise
        //they will be stuck in memory and also cause useless db queries
        DBChangeManager mgr = DBChangeManager.getMgr();
        mgr.unregister(f_det);
        mgr.unregister(f_edit);
    }
}
