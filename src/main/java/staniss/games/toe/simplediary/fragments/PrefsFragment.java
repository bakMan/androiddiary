package staniss.games.toe.simplediary.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.util.Log;

import staniss.games.toe.simplediary.R;

/** Preferences fragment
 * Created by Stanislav Smatana on 14/05/2015.
 */
public class PrefsFragment extends PreferenceFragment {

    static final String TAG = "PREF_FRAGMENT";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        Log.i(TAG, "Preferences fragment create");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "Preferences fragment resume");
    }
}
