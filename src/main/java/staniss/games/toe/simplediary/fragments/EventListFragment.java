package staniss.games.toe.simplediary.fragments;

import android.app.ListFragment;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import staniss.games.toe.simplediary.db.DBAdapter;
import staniss.games.toe.simplediary.db.DBOpenHelper;
import staniss.games.toe.simplediary.R;

/**
 * Created by Stanislav Smatana on 27/04/2015.
 */
//TODO Datum v listingu
public class EventListFragment extends ListFragment {

    final static String TAG = "EVENTLIST";
    final static String YESNO_TAG = "YESNO_DIALOG";

    //db cursor with current query
    public Cursor cursor;
    SimpleCursorAdapter adapter;

    //listener for list clicks
    private ListClickListener listener = null;

    static final int[] to = {R.id.text_name,R.id.text_description,R.id.text_date};
    static final String[] from = {DBOpenHelper.TITLE_FIELD,DBOpenHelper.DESC_FIELD,DBOpenHelper.DATE_FIELD};

    public void setCursor(Cursor c)
    {

        cursor = c;

        //if adapter is null .. it will be created with this cursor
        if(adapter != null) {
            adapter.changeCursor(c);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        adapter = new SimpleCursorAdapter(getActivity(), R.layout.list_row,cursor,from,to,0);

        //create binder to convert date and time properly
        adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                if(cursor.getColumnName(columnIndex).equals(DBOpenHelper.DATE_FIELD))
                {
                    //get title
                    String date = cursor.getString(cursor.getColumnIndex(DBOpenHelper.DATE_FIELD));
                    Calendar cal = new GregorianCalendar();

                    String date_str;
                    String time_str;

                    try
                    {
                        //parse date from db
                        Date d = DBAdapter.db_date_format.parse(date);
                        cal.setTime(d);

                        date_str = DateFormat.getDateInstance().format(cal.getTime());
                        time_str = DBAdapter.db_time_format.format(cal.getTime());

                    }catch (ParseException e)
                    {
                        Log.e(TAG,"Wrong date format in db");
                        return false;
                    }

                    //
                    String is_timed = cursor.getString(cursor.getColumnIndex(DBOpenHelper.IS_TIME_FIELD));
                    if(is_timed == null || (is_timed.equals("false") || is_timed.equals("FALSE")))
                    {
                        //all day event
                        time_str = "Whole day";
                    }

                    String txt = date_str + ", " + time_str;
                    TextView t = (TextView) view;

                    t.setText(txt);

                    return true;
                }

                return false;
            }
        });

        View view = inflater.inflate(R.layout.eventlist_fragment, null);
        setListAdapter(adapter);
        return view;

        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        registerForContextMenu(getListView());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.list_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.i(TAG,"Context menu item selected");

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId())
        {
            case R.id.menu_edit:
                listener.startEdit(info.id);
                break;

            case R.id.menu_delete:
                final long id = info.id;
                YesNoDialogFragment f =  new YesNoDialogFragment();
                f.setListener(new YesNoDialogFragment.IPositiveAnswer() {
                    @Override
                    public void action() {
                        listener.onDelete(id);
                    }
                });
                f.show(getFragmentManager(),YESNO_TAG);
                break;
        }

        return true;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        //notify listener about which id should be detailed
        if(getListener() != null)
            getListener().detail(id);
    }

    public ListClickListener getListener() {
        return listener;
    }

    public void setListener(ListClickListener listener) {
        this.listener = listener;
    }

    public interface ListClickListener
    {
        public void detail(long id);
        public void startEdit(long ID);
        public void onDelete(long ID);
    }
}
