package staniss.games.toe.simplediary.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.database.Cursor;
import android.util.Log;

/** View pager adapter containing list fragments
 * Created by Stanislav Smatana on 24/05/2015.
 */
public class SlidePagerAdapter extends FragmentPagerAdapterNS {

    String TAG = "PG_ADAPTER";

    Cursor[] cursors; //Database cursors for queries
    EventListFragment[] fragments; //Event list fragments
    String[] page_titles; //Titles of the pages

    //Listener upon clicking on the listview item
    EventListFragment.ListClickListener list_listener;

    //this fragment is shown where there are no
    //Unfortunately there have to be the same number of copies of this fragment as there is the
    //same number of eventlist fragments, otherwise FragmentManager throws exception
    NoEventsFragment[] no_events;

    public SlidePagerAdapter(Cursor[] cursors,String[] page_titles,FragmentManager fm) {
        super(fm);
        this.page_titles = page_titles;

        fragments = new EventListFragment[cursors.length];
        no_events = new NoEventsFragment[cursors.length];

        for(int cnt=0; cnt < cursors.length;cnt++)
        {
            fragments[cnt] = new EventListFragment();
            no_events[cnt] = new NoEventsFragment();
        }

        setCursors(cursors);
    }

    //Sets new set of cursors for eventlist fragments
    public void setCursors(Cursor[] new_cursors)
    {
        this.cursors = new_cursors;

        int cnt = 0;
        for(Cursor c : new_cursors)
        {
            fragments[cnt].setCursor(c);
            cnt++;
        }
    }

    @Override
    public Fragment getItem(int position) {

        //If there are results for respective query -> return fragment
        //if there are no results -> show no event fragment
        Log.i(TAG,"Request to get page at index " + Integer.toString(position));
        if(cursors[position].getCount() == 0)
            return no_events[position];
        else
            return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return page_titles[position];
    }

    public void setListListener(EventListFragment.ListClickListener lis)
    {
        list_listener = lis;
        for(EventListFragment frag : fragments)
        {
            frag.setListener(lis);
        }
    }

    @Override
    public int getItemPosition(Object object) {
        //returning POSITION_NONE ensures refresh of the ViewPager
        Log.i(TAG, "Item position requested");
        return POSITION_NONE;
    }
}
