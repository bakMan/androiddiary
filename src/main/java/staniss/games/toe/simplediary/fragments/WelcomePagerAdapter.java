package staniss.games.toe.simplediary.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.database.Cursor;

/** Pager adapter that contains single item -> welcome fragment
 * Created by Stanislav Smatana on 24/05/2015.
 */
public class WelcomePagerAdapter extends FragmentPagerAdapterNS {
    WelcomeFragment w= new WelcomeFragment();
    public WelcomePagerAdapter(WelcomeFragment.IFirstEventListener lis, FragmentManager fm) {
        super( fm);
        w.setListener(lis);
    }

    @Override
    public Fragment getItem(int position) {
        return w;
    }

    @Override
    public int getCount() {
        return 1;
    }

}
