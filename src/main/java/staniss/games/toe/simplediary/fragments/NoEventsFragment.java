package staniss.games.toe.simplediary.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import staniss.games.toe.simplediary.R;

/** Simple fragment that shows "no events" message
 * Created by Stanislav Smatana on 24/05/2015.
 */
public class NoEventsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_noevents,container,false);
    }
}
