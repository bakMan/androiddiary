package staniss.games.toe.simplediary.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Stanislav Smatana on 13/05/2015.
 */
public class DatePickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{


    public IDateChangeListener getListener() {
        return listener;
    }

    public void setListener(IDateChangeListener listener) {
        this.listener = listener;
    }

    public interface IDateChangeListener
    {
        public void onDateChange(int year, int monthOfYear,int dayOfMonth);
    }

    private IDateChangeListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        // prevent from setting past dates
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        return dialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        if(listener!=null)
            listener.onDateChange(year,monthOfYear,dayOfMonth);

    }
}
