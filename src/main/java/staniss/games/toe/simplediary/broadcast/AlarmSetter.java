package staniss.games.toe.simplediary.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

import staniss.games.toe.simplediary.db.Alarm;
import staniss.games.toe.simplediary.db.DBAdapter;
import staniss.games.toe.simplediary.db.DBOpenHelper;

/** Broadcast for setting alarms after system boot
 * Created by Stanislav Smatana on 16/05/2015.
 *
 * In android, all alarms are removed after the device is restarted or switched off and on. Therefore,
 * it is needed to set upcoming alarms on again upon system start. And that is exactly what this
 * BroadcastReciever does (It listens to BOOT broadcast)
 */
public class AlarmSetter extends BroadcastReceiver {

    static final String TAG = "ALARM_BCAST";

    @Override
    public void onReceive(Context context, Intent intent) {
        DBAdapter db = new DBAdapter(context);
        db.open();

        //get future alarms
        Cursor c = db.getFutureAlarms();

        //iterate over results
        while(c.moveToNext())
        {
            //get needed data for alarm
            String name = c.getString(c.getColumnIndex(DBOpenHelper.TITLE_FIELD));
            String time = c.getString(c.getColumnIndex(DBOpenHelper.DATE_FIELD));
            String s_id = c.getString(c.getColumnIndex(DBOpenHelper.ID_FIELD));
            String alarm_mins = c.getString(c.getColumnIndex(DBOpenHelper.ALARM_MINS_FIELD));

            //parse id field
            long id = Long.parseLong(s_id);

            //set alarm
            Alarm.alarm_set(context,id,time,alarm_mins);
            Log.i(TAG, "Setting alarm " + s_id + " " + name + " at " + time);
        }

    }
}
