package staniss.games.toe.simplediary.db.mappers;

import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

/** Sets checkbox ticked/unticked based on the boolean data provided rom db. Works both ways.
 * Created by Stanislav Smatana on 13/05/2015.
 */
public class CheckBoxSetter extends GUIElementSetter {

    final static String TAG = "CHECKBOX_DB";
    boolean inverse = false;

    public CheckBoxSetter(int id) {
        super(id);
    }

    /** Constructor
     *
     * @param id
     * @param inv If true works inverted
     */
    public CheckBoxSetter(int id,boolean inv) {
        super(id);
        inverse = inv;
    }

    @Override
    public void set(String db_data, View v) {
        CheckBox ch = (CheckBox) v.findViewById(id);
        Log.i(TAG, "Checkbox value from db: " + db_data);
        if(db_data != null && (db_data.equals("TRUE") || db_data.equals("true")))
        {
            ch.setChecked(!inverse);
        }
        else
        {
            ch.setChecked(inverse);
        }
    }

    @Override
    public String get(View v) {
        CheckBox ch = (CheckBox) v.findViewById(id);
        if(!inverse)
            return Boolean.toString(ch.isChecked());
        else
            return Boolean.toString(!ch.isChecked());
    }
}
