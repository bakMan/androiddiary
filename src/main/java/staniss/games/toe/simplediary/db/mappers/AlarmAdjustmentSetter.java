package staniss.games.toe.simplediary.db.mappers;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import staniss.games.toe.simplediary.db.DBAdapter;

/** Works in cooperation with AlarmTextSetter, sets the time to alarm information field in the detail
 * fragment, very specific use
 * Created by Stanislav Smatana on 16/05/2015.
 */
public class AlarmAdjustmentSetter extends GUIElementSetter{
    /**
     * Created by Stanislav Smatana on 15/05/2015.
     */
    AlarmTextSetter setter=null;

    public AlarmAdjustmentSetter(int id,AlarmTextSetter a) {
        super(id);
        setter = a;
    }


    @Override
    //Does the actuall setting based on calendar from AlarmTextSetter
    public void set(String db_data, View v) {

        TextView txt = (TextView) v.findViewById(id);
        int minutes;
        GregorianCalendar cal = setter.getCal();

        //parse alarm minutes
        minutes = Integer.parseInt(db_data);

        //adjust calendar date
        cal.add(Calendar.MINUTE,-minutes);

        GregorianCalendar now = new GregorianCalendar();


        long diff = (cal.getTimeInMillis() - now.getTimeInMillis())/1000;

        //if the difference is negative -> alarm is in the past
        if(diff < 0)
        {
            txt.setText("Past");
            return;
        }

        long[] dividers = {3600*24*7*4*12,3600*24*7*4,3600*24*7,3600*24,3600,60};
        String[] names_one = {"year","month","week","day","hour","minute"};
        String[] names_more = {"years","months","weeks","days","hours","minutes"};

        String output = "";

        int cnt = 0;
        for (long divider:dividers)
        {
            long tmp = Math.round((double) diff / divider);

            if(tmp != 0)
            {
                if(tmp == 1)
                {
                    output = "in " +  Long.toString(tmp) + " " + names_one[cnt];
                }
                else
                {
                    output = "in " + Long.toString(tmp) + " " + names_more[cnt];
                }

                txt.setText(output);
                return;

            }

            cnt++;
        }

        txt.setText("Now");
    }

    @Override
    public String get(View v) {
        return null;
    }
}
