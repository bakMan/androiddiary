package staniss.games.toe.simplediary.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

//TODO delete database destroy upon shipping
/** Class assists in the opening and creating of db
 * Created by Stanislav Smatana on 27/04/2015.
 */
public class DBOpenHelper extends SQLiteOpenHelper {

    private String TAG = "DB_HELPER";

    //Database and table name
    final static String DB_NAME = "bakMan.EventAppDB";
    public final static String TABLE_NAME = "events";

    //Fieldnames of db
    public final static String ID_FIELD            = "_id"; //this is requested by SimpleCursor
    public final static String TITLE_FIELD         = "title";
    public final static String DATE_FIELD          = "date";
    public final static String DESC_FIELD          = "description";
    public final static String IS_ALARM_FIELD      = "is_alarm";
    public final static String ALARM_MINS_FIELD    = "alarm_mins";
    public final static String IS_TIME_FIELD       = "is_time";

    //Create query to be executed
    final static String CREATE_QUERY =   "create table " + TABLE_NAME + " (`"+ID_FIELD+"` integer " +
                                         "primary key autoincrement, `"+TITLE_FIELD+"` text null, " +
                                         "`"+ DATE_FIELD +"` datetime null," +
                                         "`"+ DESC_FIELD +"` text null," +
                                         "`"+ IS_ALARM_FIELD +"` bool null," +
                                         "`"+ ALARM_MINS_FIELD +"` integer null," +
                                         "`"+ IS_TIME_FIELD +"` bool null);";


    public DBOpenHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "Creating tables");
        Log.i(TAG, CREATE_QUERY);
        db.execSQL(CREATE_QUERY);

        //Insert test rows
//        for (int i = 0; i <20 ; i++) {
//
//            db.execSQL("insert into "+TABLE_NAME +" VALUES ("+ Integer.toString(i) +"," +
//                    "'Testing event "+Integer.toString(i+1)+"!','2015-04-27 20:31:22','This event was created to do simple testing',0,0,0);");
//        }

    }

    @Override
    //Currently upgrade erases all the data
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO upgrade
        db.execSQL("DROP TABLE IF EXISTS " +TABLE_NAME);
        onCreate(db);
    }
}
