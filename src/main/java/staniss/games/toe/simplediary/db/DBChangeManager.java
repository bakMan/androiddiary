package staniss.games.toe.simplediary.db;

import android.content.Context;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/** Connecting component that notifies about changes in db
 * Created by Stanislav Smatana on 30/04/2015.
 */
public class DBChangeManager {

    static String TAG = "DB_CHANGE";
    DBAdapter db;
    static Context _context;

    public static void set_context(Context context)
    {
        _context = context;
    }

    public DBChangeManager()
    {
        db = new DBAdapter(_context);
        db.open();
    }

    public interface DBChangeListener
    {
        public void onDBChange(long id);
    }

    public interface DBEmptinessListener
    {
        public void onDBEmpty();
        public void onDBNonEmpty();
    }

    Map<Integer,DBChangeListener> listeners = new HashMap<>();
    Map<Integer,DBEmptinessListener> empty_listeners = new HashMap<>();
    static DBChangeManager instance = null;

    //singleton
    public static DBChangeManager getMgr()
    {
        if(instance == null)
            instance = new DBChangeManager();

        return instance;
    }

    //registers object for db change modifications
    public void register(DBChangeListener l)
    {
        listeners.put(l.hashCode(),l);
    }

    //unregisters object for db change modifications
    public void unregister(DBChangeListener l)
    {
        if(listeners.containsKey(l.hashCode()))
        {
            Log.i(TAG,"Unregistering " + l.toString());
            listeners.remove(l.hashCode());
        }
    }


    //registers for emptiness change
    public void emp_register(DBEmptinessListener l)
    {
        empty_listeners.put(l.hashCode(),l);
    }

    //unregisters for emptiness change
    public void emp_unregister(DBEmptinessListener l)
    {
        if(empty_listeners.containsKey(l.hashCode()))
        {
            Log.i(TAG,"Unregistering " + l.toString());
            empty_listeners.remove(l.hashCode());
        }
    }

    //notify listeners that there was a change in db on id
    public void notify(long id)
    {
        Log.i(TAG,"Broadcasting DB change to " + Integer.toString(listeners.size()) + " listeners");
        for(DBChangeListener lis:listeners.values())
        {
            lis.onDBChange(id);
        }

        if(db.getEventsNum() == 0)
        {
            for(DBEmptinessListener lis : empty_listeners.values())
            {
                lis.onDBEmpty();
            }

        }
        else
        {
            for(DBEmptinessListener lis : empty_listeners.values())
            {
                lis.onDBNonEmpty();
            }
        }
    }
}
