package staniss.games.toe.simplediary.db.mappers;

import android.view.View;

/** Setter sets visibility of the GUI Element based on boolean value from db
 *  DB -> GUI only
 * Created by Stanislav Smatana on 14/05/2015.
 */
public class VisibilitySetter extends GUIElementSetter {

    public VisibilitySetter(int id) {
        super(id);
    }

    @Override
    public void set(String db_data, View v) {
        View x = v.findViewById(id);

        if(db_data != null && (db_data.equals("true") || db_data.equals("TRUE")))
        {
            x.setVisibility(View.VISIBLE);
        }
        else
        {
            x.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public String get(View v) {
        return null;
    }
}
